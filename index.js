const express = require("express")
const app = express()
const _PORT = "3000"

app.use(express.json())

app.get('/',(req,res)=>{
    res.json({"message":"hi"})
})


app.listen(_PORT,()=>{
    console.log('Server starting in '+_PORT)
})


